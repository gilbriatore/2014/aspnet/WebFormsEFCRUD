﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class CadastrarFornecedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            Contexto ctx = new Contexto();
            Fornecedor f = new Fornecedor();

            f.Nome = txtNome.Text;
            f.Telefone = txtTelefone.Text;

            ctx.Fornecedores.Add(f);
            ctx.SaveChanges();

            txtTelefone.Text = "";
            txtNome.Text = "";
        }
    }
}