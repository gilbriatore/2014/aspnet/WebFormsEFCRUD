﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AlterarProduto.aspx.cs" Inherits="WebFormsEFCRUD.AlterarProduto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Table ID="Table1" runat="server">

        <asp:TableRow ID="TableRow4" runat="server">
            <asp:TableCell ID="TableCell6" runat="server">Nome:</asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server">
                <asp:TextBox ID="txtConsultar" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow5" runat="server">
            <asp:TableCell ID="TableCell8" runat="server" ColumnSpan="2" HorizontalAlign="Right">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click"/>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">Nome:</asp:TableCell>
            <asp:TableCell ID="TableCell2" runat="server">
                <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell3" runat="server">Telefone:</asp:TableCell>
            <asp:TableCell ID="TableCell4" runat="server">
                <asp:TextBox ID="txtQuantidade" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow ID="TableRow6" runat="server">
            <asp:TableCell ID="TableCell9" runat="server">Fornecedor:</asp:TableCell>
            <asp:TableCell ID="TableCell10" runat="server">
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Nome" DataValueField="Id"></asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell5" runat="server" ColumnSpan="2" HorizontalAlign="Right">
                <asp:Button ID="btnAlterar" runat="server" Text="Alterar" OnClick="btnAlterar_Click"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WebFormsEFCRUD.ContextoConnectionString %>" SelectCommand="SELECT * FROM [Fornecedors]"></asp:SqlDataSource>
</asp:Content>
