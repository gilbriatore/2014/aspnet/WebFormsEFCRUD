﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class CadastrarProduto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            Contexto ctx = new Contexto();
            Produto p = new Produto();

            p.Nome = txtNome.Text;
            p.Quantidade = int.Parse(txtQuantidade.Text);
            int id = int.Parse(DropDownList1.SelectedValue);
            p.Fornecedor = ctx.Fornecedores.SingleOrDefault(x => x.Id == id);

            ctx.Produtos.Add(p);
            ctx.SaveChanges();

            txtQuantidade.Text = "";
            txtNome.Text = "";
            DropDownList1.SelectedIndex = -1;
        }
    }
}