﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsEFCRUD
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public virtual Fornecedor Fornecedor { get; set; }
    }
}