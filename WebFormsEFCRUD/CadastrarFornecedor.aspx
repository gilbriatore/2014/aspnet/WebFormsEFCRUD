﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CadastrarFornecedor.aspx.cs" Inherits="WebFormsEFCRUD.CadastrarFornecedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Table ID="Table1" runat="server">
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Nome:</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">Telefone:</asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="txtTelefone" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server" ColumnSpan="2" HorizontalAlign="Right">
                <asp:Button ID="btnGravar" runat="server" Text="Gravar" OnClick="btnGravar_Click"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    
</asp:Content>
