﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class Venda : System.Web.UI.Page
    {
        Contexto ctx = new Contexto();
        static Cliente c = new Cliente();
        static List<Cliente> clientes = new List<Cliente>();

        protected void Page_Load(object sender, EventArgs e)
        {
            clientes = ctx.Clientes.ToList();
            GridView1.DataSource = clientes;
            GridView1.DataBind();
        }

        public List<Cliente> getClientes()
        {
            return clientes;
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            c = ctx.Clientes.FirstOrDefault(x => x.Nome.Equals(txtConsultar.Text));
            if (c != null)
            {
                txtNome.Text = c.Nome;
                txtTelefone.Text = c.Telefone;
            }
        }
        
        protected void btnRemover_Click(object sender, EventArgs e)
        {
            c.Nome = txtNome.Text;
            c.Telefone = txtTelefone.Text;

            ctx.Entry(c).State = EntityState.Deleted;
            ctx.SaveChanges();

            txtTelefone.Text = "";
            txtNome.Text = "";
            txtConsultar.Text = "";
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            //GridView1.Rows[1].Cells.Add(new TableCell().Text
        }
    }
}