﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Venda.aspx.cs" Inherits="WebFormsEFCRUD.Venda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Table ID="Table1" runat="server">

        <asp:TableRow ID="TableRow4" runat="server">
            <asp:TableCell ID="TableCell6" runat="server">Nome:</asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server">
                <asp:TextBox ID="txtConsultar" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow5" runat="server">
            <asp:TableCell ID="TableCell8" runat="server" ColumnSpan="2" HorizontalAlign="Right">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click"/>
            </asp:TableCell>
        </asp:TableRow>
        

        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">Nome:</asp:TableCell>
            <asp:TableCell ID="TableCell2" runat="server">
                <asp:TextBox ID="txtNome" runat="server" Enabled="false"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell3" runat="server">Telefone:</asp:TableCell>
            <asp:TableCell ID="TableCell4" runat="server">
                <asp:TextBox ID="txtTelefone" runat="server" Enabled="false"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>

        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell10" runat="server" HorizontalAlign="Right">Quantidade:</asp:TableCell>
            <asp:TableCell ID="TableCell9" runat="server" ColumnSpan="2">
                <asp:TextBox ID="txtQuantidade" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="TableCell5" runat="server">
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Nome" DataValueField="Id"></asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell ID="TableCell11" runat="server" ColumnSpan="2" HorizontalAlign="Right">
                <asp:Button ID="btnAdicionar" runat="server" Text="Adicionar" OnClick="btnAdicionar_Click"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WebFormsEFCRUD.ContextoConnectionString %>" SelectCommand="SELECT * FROM [Produtoes]"></asp:SqlDataSource>
    
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
</asp:Content>