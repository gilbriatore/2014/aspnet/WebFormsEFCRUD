﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ListarProduto.aspx.cs" Inherits="WebFormsEFCRUD.ListarProduto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" HorizontalAlign="Center" Width="50%">
    <Columns>
        <asp:BoundField DataField="NomeProdu" HeaderText="Produto" SortExpression="NomeProdu" />
        <asp:BoundField DataField="Quantidade" HeaderText="Quantidade" SortExpression="Quantidade" />
        <asp:BoundField DataField="NomeForn" HeaderText="Fornecedor" SortExpression="NomeForn" ItemStyle-Width="20%"/>
    </Columns>
</asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:WebFormsEFCRUD.ContextoConnectionString %>" SelectCommand="SELECT Fornecedors.Nome AS NomeForn, Produtoes.Nome AS NomeProdu, Produtoes.Quantidade, Produtoes.Id FROM Fornecedors INNER JOIN Produtoes ON Fornecedors.Id = Produtoes.Fornecedor_Id"></asp:SqlDataSource>
</asp:Content>
