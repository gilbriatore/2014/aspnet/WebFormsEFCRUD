﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class Alterar : System.Web.UI.Page
    {
        Contexto ctx = new Contexto();
        static Cliente c = new Cliente();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            c = ctx.Clientes.FirstOrDefault(x => x.Nome.Equals(txtConsultar.Text));
            if (c != null)
            {
                txtNome.Text = c.Nome;
                txtTelefone.Text = c.Telefone;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            c.Nome = txtNome.Text;
            c.Telefone = txtTelefone.Text;

            ctx.Entry(c).State = EntityState.Modified;
            ctx.SaveChanges();

            txtTelefone.Text = "";
            txtNome.Text = "";
            txtConsultar.Text = "";
        }
    }
}