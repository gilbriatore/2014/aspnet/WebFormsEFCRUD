﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class RemoverProduto : System.Web.UI.Page
    {
        static Contexto ctx = new Contexto();
        static Produto p = new Produto();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            p = ctx.Produtos.Include("Fornecedor").FirstOrDefault(x => x.Nome.Equals(txtConsultar.Text));
            if (p != null)
            {
                txtNome.Text = p.Nome;
                txtQuantidade.Text = p.Quantidade.ToString();
                DropDownList1.SelectedValue = p.Fornecedor.Id.ToString();
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            p.Nome = txtNome.Text;
            p.Quantidade = int.Parse(txtQuantidade.Text);
            int id = int.Parse(DropDownList1.SelectedValue);
            p.Fornecedor = ctx.Fornecedores.SingleOrDefault(x => x.Id == id);

            ctx.Produtos.Remove(p);
            ctx.SaveChanges();

            txtQuantidade.Text = "";
            txtNome.Text = "";
            txtConsultar.Text = "";
        }
    }
}