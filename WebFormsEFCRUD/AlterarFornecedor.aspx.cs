﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsEFCRUD
{
    public partial class AlterarFornecedor : System.Web.UI.Page
    {
        Contexto ctx = new Contexto();
        static Fornecedor f = new Fornecedor();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            f = ctx.Fornecedores.FirstOrDefault(x => x.Nome.Equals(txtConsultar.Text));
            if (f != null)
            {
                txtNome.Text = f.Nome;
                txtTelefone.Text = f.Telefone;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            f.Nome = txtNome.Text;
            f.Telefone = txtTelefone.Text;

            ctx.Entry(f).State = EntityState.Modified;
            ctx.SaveChanges();

            txtTelefone.Text = "";
            txtNome.Text = "";
            txtConsultar.Text = "";
        }
    }
}